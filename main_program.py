from PyQt5 import QtWidgets, uic

import sys
sys.path.append('./gui/')

import xradio_viewer

app = QtWidgets.QApplication(sys.argv)
viewer = xradio_viewer.MainGUI()
app.exec_()