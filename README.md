<img src="feup.png" style="zoom: 20%;" />

## What is XRadio?

​	XRadio is a final course project, build by a team of 40 students, for *SETEC, Engineering Systems - Telecommunications, Electronics and Computers* (EEC0140), at *FEUP, Faculty of Engineering - University of Porto*.

​	Modern cameras are an engineering marvel and can be used in a lot of situations, but they fail when using them to see through walls, fog or at night. These missing features could be used in rescue missions, scientific research and autonomous vehicles. X-Radio focuses exactly on this type of problem. It uses radio-frequency waves to map the environment around and detect moving objects in a 3D representation.

## What is SETEC?

​	*SETEC, Engineering Systems - Telecommunications, Electronics and Computers* (EEC0140) aims to give the students the opportunity to develop and integrate a complex system in an environment that closely resembles what they will find in real-life business. The system must comply with the engineering requirements that are extracted from the end user functional requirements for the application.

## Contributors

|Name|TEAM|E-mail|Role|
|-|-|-|-|
|Dany Teixeira|Interfaces|up201504166@fe.up.pt|Team Leader
|Pedro Magalhães|Interfaces|up201504174@fe.up.pt|Team Sub Leader
|Carla Jorge|Interfaces|up201505033@fe.up.pt
|Diogo Correia|Interfaces|up201504726@fe.up.pt
|Edgar Matos|Interfaces|up201405788@fe.up.pt
|Joana Macedo|Interfaces|up201607759@fe.up.pt
|João Barbosa|Interfaces|up201405198@fe.up.pt
|Jorge Queirós|Interfaces|up201808903@fe.up.pt
|Luís Ribeiro|Interfaces|up201505220@fe.up.pt
|Miguel Cardoso|Interfaces|ee11059@fe.up.pt
|Pedro Silva|Interfaces|up201003016@fe.up.pt
|Ricardo Trancoso|Interfaces|up201505763@fe.up.pt
|Rubén Queirós|Interfaces|up201504489@fe.up.pt

## External Links

* [University of Porto Website](https://sigarra.up.pt)

* [Faculty of Engineering Website](https://sigarra.up.pt/feup/)

* [SETEC Course WebPage](https://sigarra.up.pt/feup/en/UCURR_GERAL.FICHA_UC_VIEW?pv_ocorrencia_id=420347)