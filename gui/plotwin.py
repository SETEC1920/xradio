from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL.shaders import *
import scipy.io as scio
import numpy as np
import time 
import sys

global COORDS, COLORS, X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW, MODE, VERTEX_VBO, COLOR_VBO
global COORDS_WIRE, COLORS_WIRE, VERTEX_VBO_WIRE, COLOR_VBO_WIRE

def display_data_gpu():
	
	global X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW, VERTEX_VBO, COLOR_VBO, COORDS
	AreaX , AreaY, AreaZ = 4.0/2.0, 3.0/2.0, 3.0/2.0 

	glGetError()
	#glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	glLoadIdentity()

	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_VBO)
	glBufferData(GL_ARRAY_BUFFER, COORDS, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, COLOR_VBO)
	glBufferData(GL_ARRAY_BUFFER, COLORS, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, 0)

	# Performing translation and rotation
	glTranslatef(X_AXIS, Y_AXIS, Z_AXIS)
	glTranslatef(AreaX, AreaY, -AreaZ)
	glRotatef(PITCH, 1.0, 0.0, 0.0)
	glRotatef(ROLL, 0.0, 1.0, 0.0)
	glRotatef(YAW, 0.0, 0.0, 1.0)
	glTranslatef(-AreaX, -AreaY, AreaZ)

	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_VBO)
	glVertexPointer(3, GL_FLOAT, 0, None)
	glEnableClientState(GL_VERTEX_ARRAY)
	glBindBuffer(GL_ARRAY_BUFFER, COLOR_VBO)
	glColorPointer(4, GL_FLOAT, 0, None)
	glEnableClientState(GL_COLOR_ARRAY)
	glBindBuffer(GL_ARRAY_BUFFER, 0)

	# Define buffer size as
	# no. of vertices * no. of faces * no. of cubes
	#glDrawArrays(GL_QUADS, 0, 4*6*len(COORDS))
	glDrawArrays(GL_QUADS, 0, len(COORDS))
	#glDrawArrays(GL_LINE_STRIP, len(COORDS)-24, len(COORDS))
	glDisableClientState(GL_VERTEX_ARRAY)
	glDisableClientState(GL_COLOR_ARRAY)
	#glFlush()
	#glutSwapBuffers()

def display_cube_culling():
		
	global X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW, VERTEX_VBO_WIRE, COLOR_VBO_WIRE, COORDS_WIRE
	AreaX , AreaY, AreaZ = 4.0/2.0, 3.0/2.0, 3.0/2.0 
	glGetError()
	#glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	glLoadIdentity()

	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_VBO_WIRE)
	glBufferData(GL_ARRAY_BUFFER, COORDS_WIRE, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, COLOR_VBO_WIRE)
	glBufferData(GL_ARRAY_BUFFER, COLORS_WIRE, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, 0)

	glEnable(GL_CULL_FACE)
	glCullFace(GL_BACK)
	glFrontFace(GL_CW)

	# Performing translation and rotation
	glTranslatef(X_AXIS, Y_AXIS, Z_AXIS)
	glTranslatef(AreaX, AreaY, -AreaZ)
	glRotatef(PITCH, 1.0, 0.0, 0.0)
	glRotatef(ROLL, 0.0, 1.0, 0.0)
	glRotatef(YAW, 0.0, 0.0, 1.0)
	glTranslatef(-AreaX, -AreaY, AreaZ)


	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_VBO_WIRE)
	glVertexPointer(3, GL_FLOAT, 0, None)
	glEnableClientState(GL_VERTEX_ARRAY)
	glBindBuffer(GL_ARRAY_BUFFER, COLOR_VBO_WIRE)
	glColorPointer(4, GL_FLOAT, 0, None)
	glEnableClientState(GL_COLOR_ARRAY)
	glBindBuffer(GL_ARRAY_BUFFER, 0)
	
	glDrawArrays(GL_QUADS, 0, len(COORDS_WIRE))
	glDisableClientState(GL_VERTEX_ARRAY)
	glDisableClientState(GL_COLOR_ARRAY)

	glDisable(GL_CULL_FACE)
	#glFlush()
	#glutSwapBuffers()

def display_gpu():
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	
	display_cube_culling()
	display_data_gpu()
	
	glFlush()
	#glutSwapBuffers()


def init_camera_position():
	global COORDS, X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW
	# Calculate centroid for all vertices across cubes in each axis
	#X_AXIS /= len(COORDS)
	X_AXIS = -2.0
	#Y_AXIS /= len(COORDS)
	Y_AXIS = -1.0
	#Z_AXIS /= len(COORDS)
	Z_AXIS = -6.0
	#Z_AXIS -= 10.0
	PITCH = 35.0
	ROLL = -35.0
	YAW = 0.0	

def generate_cube_vertices_gpu(fullData, res):

	global COORDS, COLORS

	coords_arr = []
	colors_arr = []
	# Add 8 vertices in cube, colors for each vertex
	for x, y, z, r, g, b, a in fullData:
		#X_AXIS += x
		#Y_AXIS += y
		#Z_AXIS += z
		vertex_1 = [x, y, z]
		vertex_2 = [x+res, y, z]
		vertex_3 = [x+res, y+res, z]
		vertex_4 = [x, y+res, z]
		vertex_5 = [x, y, z+res]
		vertex_6 = [x+res, y, z+res]
		vertex_7 = [x+res, y+res, z+res]
		vertex_8 = [x, y+res, z+res]

		coords_arr.extend([vertex_1, vertex_2, vertex_3, vertex_4])
		coords_arr.extend([vertex_5, vertex_6, vertex_7, vertex_8])
		coords_arr.extend([vertex_1, vertex_4, vertex_8, vertex_5])
		coords_arr.extend([vertex_1, vertex_3, vertex_7, vertex_6])
		coords_arr.extend([vertex_1, vertex_2, vertex_6, vertex_5])
		coords_arr.extend([vertex_4, vertex_3, vertex_7, vertex_8])
		for x in range(24):
			colors_arr.append([r, g, b, a])

	COORDS = np.array(coords_arr, dtype=np.float32)
	COLORS = np.array(colors_arr, dtype=np.float32)

def generate_cube_culling():

	global COORDS_WIRE, COLORS_WIRE
	coords_arr = []
	colors_arr = []

	# Wireframe - Area wireframe poly vertices
	vertex_1 = [-0.2, -0.2, -3.2]
	vertex_2 = [4.2, -0.2, -3.2]
	vertex_3 = [4.2, 3.2, -3.2]
	vertex_4 = [-0.2, 3.2, -3.2]
	vertex_5 = [-0.2, -0.2, 0.2]
	vertex_6 = [4.2, -0.2, 0.2]
	vertex_7 = [4.2, 3.2, 0.2]
	vertex_8 = [-0.2, 3.2, 0.2]

	coords_arr.extend([vertex_5, vertex_6, vertex_7, vertex_8])
	coords_arr.extend([vertex_6, vertex_2, vertex_3, vertex_7])
	coords_arr.extend([vertex_2, vertex_1, vertex_4, vertex_3])
	coords_arr.extend([vertex_1, vertex_5, vertex_8, vertex_4])
	coords_arr.extend([vertex_1, vertex_2, vertex_6, vertex_5])
	coords_arr.extend([vertex_8, vertex_7, vertex_3, vertex_4])
	for c in range(24):
		colors_arr.append([0.15, 0.15, 0.15, 1.0])
	# Wireframe
	COORDS_WIRE = np.array(coords_arr, dtype=np.float32)
	COLORS_WIRE = np.array(colors_arr, dtype=np.float32)


def onKeyDown(key, *args):
	global X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW
	# Retrieving key events and storing translation/rotation values
	#key = args[0].decode("utf-8")
	AXISINCREMENT = 0.1
	PITCHROLLINCREMENT = 1.0
	if key == 'q':
		sys.exit()
	elif key == 'w':
		Y_AXIS += AXISINCREMENT
	elif key == 's':
		Y_AXIS -= AXISINCREMENT
	elif key == 'a':
		X_AXIS -= AXISINCREMENT
	elif key == 'd':
		X_AXIS += AXISINCREMENT
	elif key == 'z':
		Z_AXIS += AXISINCREMENT
	elif key == 'x':
		Z_AXIS -= AXISINCREMENT
	elif key == 'i':
		YAW -= PITCHROLLINCREMENT
	elif key == 'k':
		YAW += PITCHROLLINCREMENT
	elif key == 'j':
		PITCH += PITCHROLLINCREMENT
	elif key == 'l':
		PITCH -= PITCHROLLINCREMENT
	elif key == 'n':
		ROLL += PITCHROLLINCREMENT
	elif key == 'm':
		ROLL -= PITCHROLLINCREMENT
	# Clamping rotation
	np.clip(ROLL, -360.0, 360.0)
	np.clip(PITCH, -360.0, 360.0)
	np.clip(YAW, -360.0, 360.0)
	# Re-render
	#display_gpu()

def onMouse(key, *args):
	global X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW
	# Retrieving key events and storing translation/rotation values
	#key = args[0].decode("utf-8")
	AXISINCREMENT = 0.35
	PITCHROLLINCREMENT = 2
	if key == 's':
		PITCH -= PITCHROLLINCREMENT
	elif key == 'n':
		PITCH += PITCHROLLINCREMENT
	elif key == 'o':
		ROLL -= PITCHROLLINCREMENT
	elif key == 'e':
		ROLL += PITCHROLLINCREMENT
	elif key == 'se':
		ROLL += PITCHROLLINCREMENT
		PITCH -= PITCHROLLINCREMENT
	elif key == 'ne':
		ROLL += PITCHROLLINCREMENT
		PITCH += PITCHROLLINCREMENT
	elif key == 'so':
		ROLL -= PITCHROLLINCREMENT
		PITCH -= PITCHROLLINCREMENT
	elif key == 'no':
		ROLL -= PITCHROLLINCREMENT
		PITCH += PITCHROLLINCREMENT
	elif key == 'scrollup':
		Z_AXIS += AXISINCREMENT
	elif key == 'scrolldown':
		Z_AXIS -= AXISINCREMENT

	# Clamping rotation
	np.clip(ROLL, -360.0, 360.0)
	np.clip(PITCH, -360.0, 360.0)
	np.clip(YAW, -360.0, 360.0)
	# Re-render
	#display_gpu()


def filterData(data, res, gradScale, colorMap1, colorMap2, *args, **kwargs):

	# Get data non Zero sim data scaled by factor
	tmpx, tmpy, tmpz = [None]*gradScale, [None]*gradScale, [None]*gradScale
	x, y, z = [None]*gradScale, [None]*gradScale, [None]*gradScale

	for i in range(0, gradScale):
		tmpx[i], tmpy[i], tmpz[i] = np.nonzero(np.logical_and(data > (i/gradScale), data <= ((i+1)/gradScale)))
		# Change orientation Z axis is Y axis / Change Z direction into negative realm
		x[i], y[i], z[i] = tmpx[i]*res, tmpz[i]*res, tmpy[i]*res*(-1)

	# Pos and conconcatenation measured data
	pos = [None]*gradScale
	for i in range(0, gradScale):
		pos[i] = np.c_[x[i], y[i], z[i]]

	# Color settings
	gradSteps = colorMap1 - colorMap2
	colorData = [None]*gradScale
	for i in range(0, gradScale):
		colorData[i] = np.full((len(pos[i]),4),((colorMap2[0]+((gradSteps[0]/gradScale)*(i+1)))/255, 
												(colorMap2[1]+((gradSteps[1]/gradScale)*(i+1)))/255, 
												(colorMap2[2]+((gradSteps[2]/gradScale)*(i+1)))/255,(i+1)/gradScale))
	# Exponencial 
	# np.power((i+1)/gradScale, 2)
	
	# Concatenation all information
	data = [None]*gradScale
	for i in range(0, gradScale):
		data[i] = np.c_[pos[i], colorData[i]]

	return data

def initOpengl():

	global X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW, VERTEX_VBO_WIRE, COLOR_VBO_WIRE, VERTEX_VBO, COLOR_VBO
	X_AXIS, Y_AXIS, Z_AXIS, PITCH, ROLL, YAW = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
	VERTEX_VBO_WIRE, COLOR_VBO_WIRE = glGenBuffers(2)
	VERTEX_VBO, COLOR_VBO = glGenBuffers(2)
	init_camera_position()


def oldGlutLoop(data):
	
	global COORDS, COLORS, VERTEX_VBO, COLOR_VBO
	global COORDS_WIRE, COLORS_WIRE, VERTEX_VBO_WIRE, COLOR_VBO_WIRE
	COORDS, COLORS = 0.0, 0.0
	COORDS_WIRE, COLORS_WIRE = 0.0, 0.0
	# Resolution 
	res = 0.2	
	# Set gradScale to edit grandient/steps
	gradScale = 10
	# Starting RGB gradient
	colorMap1 = np.array([220, 20, 60])
	colorMap2 = np.array([73, 7, 190])

	# Filter data
	fullData = filterData(data, res, gradScale, colorMap1, colorMap2)	
	# Set minium treshol, ex tresholdMin = 1  --> z > tresholdMin/gradScale
	tresholdMin = 1
	drawData = fullData[tresholdMin]
	#print(fullData[1])
	for i in range(tresholdMin, gradScale-1):
		drawData = np.concatenate([drawData, fullData[i+1]], axis=0)
	# Flip DrawData for better blending alpha options
	generate_cube_vertices_gpu(np.flip(drawData, axis=0), res)
	#generate_wireframe()
	generate_cube_culling()
	calculate_centroid()
	glutDisplayFunc(display_gpu)

	glutKeyboardFunc(onKeyDown)
	# Alpha channel
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
	# Test this shit func
	# glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO)
	glEnable(GL_BLEND)
	# Background
	glClearColor(0.13, 0.13, 0.13, 1.0)
	glClearDepth(1.0)
	glDepthFunc(GL_LESS)

	glEnable(GL_DEPTH_TEST)
	glShadeModel(GL_SMOOTH)
	glMatrixMode(GL_PROJECTION)
	glLoadIdentity()
	gluPerspective(60.0, 1280.0 / 720.0, 0.1, 100.0)
	glMatrixMode(GL_MODELVIEW)

	# Setup buffers for parallelization
	VERTEX_VBO, COLOR_VBO = glGenBuffers(2)
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_VBO)
	glBufferData(GL_ARRAY_BUFFER, COORDS, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, COLOR_VBO)
	glBufferData(GL_ARRAY_BUFFER, COLORS, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, 0)
	# Setup buffers for parallelization wireframe
	VERTEX_VBO_WIRE, COLOR_VBO_WIRE = glGenBuffers(2)
	glBindBuffer(GL_ARRAY_BUFFER, VERTEX_VBO_WIRE)
	glBufferData(GL_ARRAY_BUFFER, COORDS_WIRE, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, COLOR_VBO_WIRE)
	glBufferData(GL_ARRAY_BUFFER, COLORS_WIRE, GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, 0)
	#glutMainLoop()