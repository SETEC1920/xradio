from PyQt5 import uic, QtWidgets, QtCore, QtGui
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from plotwin import *
import scipy.io as scio
import numpy as np
import time 
import sys

class MainGUI(QtWidgets.QMainWindow):
    
	def __init__(self):
		super(MainGUI, self).__init__()
		uic.loadUi('./gui/mainwindow.ui', self)
		self.a = AboutWin()

		#Components
		self.connectbtn = self.findChild(QtWidgets.QPushButton, 'connect_btn')
		self.playpausebtn = self.findChild(QtWidgets.QPushButton, 'playpause_btn')
		self.resetbtn = self.findChild(QtWidgets.QPushButton, 'reset_btn')
		self.calibratebtn = self.findChild(QtWidgets.QPushButton, 'calibrate_btn')
		self.aboutbtn = self.findChild(QtWidgets.QPushButton, 'about_btn')
		self.glarea = self.findChild(QtWidgets.QOpenGLWidget, 'GLArea')

		mat = scio.loadmat('gui/interfaces.mat')
		self.data = np.array(mat['measured_data'])

		self.glarea.initializeGL()
		self.glarea.initializeGL = self.initializeGL
		self.glarea.paintGL = self.paintGL
		self.glarea.resizeGL = self.resizeGL
		self.mousePos = 0

		timer = QtCore.QTimer(self)
		timer.timeout.connect(self.glarea.update) 
		timer.start(10)

		# Data suffle timer
		timerData = QtCore.QTimer(self)
		timerData.timeout.connect(self.suffleData)
		timerData.start(250)

		#Component Behavior
		self.connectbtn.clicked.connect(self.on_connectbtn)
		self.playpausebtn.clicked.connect(self.on_playpausebtn)
		self.resetbtn.clicked.connect(self.on_resetbtn)
		self.calibratebtn.clicked.connect(self.on_calibratebtn)
		self.aboutbtn.clicked.connect(self.on_aboutbtn)

		#Button States
		self.isConnected = False
		self.isPlay = False

		self.show()

	def initializeGL(self):

		glutInit("gpu")
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH)
		glutKeyboardFunc(onKeyDown)
		# Alpha channel
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
		# Test this shit func
		# glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO)
		glEnable(GL_BLEND)
		# Background
		glClearColor(0.13, 0.13, 0.13, 1.0)
		glClearDepth(1.0)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)

		# Init global camera position
		initOpengl()

	def paintGL(self):

		# Resolution 
		res = 0.2	
		# Set gradScale to edit grandient/steps
		gradScale = 10
		# Starting RGB gradient
		colorMap1 = np.array([220, 20, 60])
		colorMap2 = np.array([73, 7, 190])

		# Filter data
		fullData = filterData(self.data, res, gradScale, colorMap1, colorMap2)	
		# Set minium treshol, ex tresholdMin = 1  --> z > tresholdMin/gradScale
		tresholdMin = 1
		drawData = fullData[tresholdMin]
		#print(fullData[1])
		for i in range(tresholdMin, gradScale-1):
			drawData = np.concatenate([drawData, fullData[i+1]], axis=0)
		# Flip DrawData for better blending alpha options
		generate_cube_vertices_gpu(np.flip(drawData, axis=0), res)
		#generate_wireframe()
		generate_cube_culling()
		#calculate_centroid()
		display_gpu()
	
	def resizeGL(self, width, height):
		side = min(width, height)
		glViewport((width - side) // 2, (height - side) // 2, side, side)
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45.0, 1920.0 / 1080.0, 0.01, 100.0)
		glMatrixMode(GL_MODELVIEW)

	def keyPressEvent(self, event):
		"""
		if event.key() == QtCore.Qt.Key_Q:
			print(event.text())
		elif event.key() == QtCore.Qt.Key_Enter:
			print(event.key())
		"""
		#print(event.text())
		onKeyDown(event.text())

	def mousePressEvent(self, event):
		if event.button() == QtCore.Qt.LeftButton:
			self.mousePos = event.pos()

	def mouseMoveEvent(self, event):
		if(event.buttons() & QtCore.Qt.LeftButton):
			offset = event.pos() - self.mousePos
			#tan = offset.y/offset.x
			#ang = np.arctan(tan) 
			x = offset.x()
			y = offset.y()
			ang = np.arctan2(x,y) + np.pi/2
			if(ang >= -np.pi/8 and ang < np.pi/8):
				onMouse('o')
			elif(ang >= np.pi/8 and ang < 3*np.pi/8):
				onMouse('no')
			elif(ang >= 3*np.pi/8 and ang < 5*np.pi/8):
				onMouse('n')
			elif(ang >= 5*np.pi/8 and ang < 7*np.pi/8):
				onMouse('ne')
			elif(ang >= 7*np.pi/8 and ang < 9*np.pi/8):
				onMouse('e')
			elif(ang >= 9*np.pi/8 and ang < 11*np.pi/8):
				onMouse('se')
			elif(ang >= 11*np.pi/8 and ang < 13*np.pi/8):
				onMouse('s')
			elif(ang >= -3*np.pi/8 and ang < -np.pi/8):
				onMouse('so')
	
	def wheelEvent(self, event):
		if(event.angleDelta().y() > 0):
			onMouse('scrollup')
		elif (event.angleDelta().y() < 0):
			onMouse('scrolldown')

	def suffleData(self):
		np.random.shuffle(self.data)

	#On components call
	def on_connectbtn(self):
		if self.isConnected: 
			self.connectbtn.setText("Connect")
			self.isConnected = False
		else:
			self.connectbtn.setText("Disconnect")
			self.isConnected = True
		#print("CONNECT/DISCONNECT")

	def on_playpausebtn(self):
		if self.isPlay: 
			self.playpausebtn.setText("Play")
			self.playpausebtn.setIcon(QtGui.QIcon("./files/play.png"))
			self.isPlay = False
		else:
			self.playpausebtn.setText("Pause")
			self.playpausebtn.setIcon(QtGui.QIcon("./files/pause.png"))
			self.isPlay = True

	def on_resetbtn(self):
		init_camera_position()

	def on_calibratebtn(self):
		print("CALIBRATE")
		
	def on_aboutbtn(self):
		self.a.winshow()

class AboutWin(QtWidgets.QDialog):
	def __init__(self):
		super(AboutWin, self).__init__() # Call the inherited classes __init__ method
		uic.loadUi('./gui/about.ui', self) # Load the .ui file

	def winshow(self):
		self.show()